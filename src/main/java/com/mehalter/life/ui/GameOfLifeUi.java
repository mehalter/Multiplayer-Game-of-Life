package com.mehalter.life.ui;

import com.mehalter.life.GameOfLife;
import com.mehalter.life.model.GameState;
import com.mehalter.life.persistence.GameOfLifeFileWriter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

// Defines the User Interface
public class GameOfLifeUi extends JFrame {

    private static final long serialVersionUID = -6912109370693886742L;

    private GameOfLife gameOfLife;
    private GameState gameState;
    private GridPanel gridPanel;
    private JPopupMenu shapeMenu;
    private GameOfLifeFileWriter fileWriter;

    public GameOfLifeUi(GameOfLife gameOfLife, GameState gameState,
            GridPanel gridPanel) {
        this.gameOfLife = gameOfLife;
        this.gameState = gameState;
        this.gridPanel = gridPanel;
        this.fileWriter = new GameOfLifeFileWriter(this, gameState);
    }

    public JPopupMenu getShapeMenu() {
        return shapeMenu;
    }

    public GridPanel getGridPanel() {
        return gridPanel;
    }

    public void makeMenus() {
        JMenuBar mbar = new JMenuBar();
        shapeMenu = new JPopupMenu("Shapes");
        setJMenuBar(mbar);

        // build the menus in the menu bar
        JMenu sizeMenu = buildSizeMenu();
        JMenu speedMenu = buildSpeedMenu();
        JMenu optionMenu = buildOptionMenu();
        JMenu fileMenu = buildFileMenu();

        // initialize items in the menu bar
        JMenuItem startItem = new JMenuItem("Start");
        JMenuItem stepItem = new JMenuItem("Step");
        JMenuItem userItem = new JMenuItem("User: 1");
        JMenuItem colorItem = new JMenuItem("Color");
        JMenuItem clearItem = new JMenuItem("Clear");

        // set keyboard shortcuts for menu items
        startItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0));
        stepItem.setAccelerator(KeyStroke.getKeyStroke("N"));
        userItem.setAccelerator(KeyStroke.getKeyStroke("U"));
        colorItem.setAccelerator(KeyStroke.getKeyStroke("C"));
        clearItem.setAccelerator(KeyStroke.getKeyStroke("Q"));

        // define actions of each menu item
        // stepItem increases a single step
        stepItem.addActionListener(e -> gameOfLife.nextStep());
        // userItem changes the user and resets the button text to reflect the
        // current user
        userItem.addActionListener(e -> {
            if (gameState.isSecond())
                gameState.switchCurrentUser();
            // if all second strain rules are off, keep user at user 1
            else if (gameState.getCurrentUser() != 1)
                gameState.switchCurrentUser();
            userItem.setText("User: " + gameState.getCurrentUser());
        });
        // asks user to pick a color to change the current user to
        colorItem.addActionListener(e -> {
            Color tempColor = JColorChooser.showDialog(gridPanel,
                    "Choose a Color",
                    gameState.getCurrentUser() == 1 ? gameState.getUser1()
                            : gameState.getUser2());
            if (tempColor != null) {
                if (gameState.getCurrentUser() == 1)
                    gameState.setUser1(tempColor);
                else
                    gameState.setUser2(tempColor);
            }
            gridPanel.repaint();
        });
        // creates a new grid
        clearItem.addActionListener(e -> {
            int[][] grid = new int[gameState.getySize()][gameState.getxSize()];
            gameState.setGrid(grid);
            gridPanel.repaint();
        });
        // starts the game continuously
        startItem.addActionListener(e -> {
            // reverse the running boolean
                gameState.setRunning(!gameState.isRunning());
                startItem.setText(gameState.isRunning() ? "Stop" : "Start");

                // while running, user cannot be able to click other buttons
                stepItem.setEnabled(!gameState.isRunning());
                clearItem.setEnabled(!gameState.isRunning());
                userItem.setEnabled(!gameState.isRunning());
                colorItem.setEnabled(!gameState.isRunning());
                sizeMenu.setEnabled(!gameState.isRunning());
                speedMenu.setEnabled(!gameState.isRunning());

                // start animation
                if (gameState.isRunning()) {
                    gameState.getGameTimer().start();
                } else {
                    gameState.getGameTimer().stop();
                }
            });

        // add menu items and menus
        mbar.add(fileMenu);
        mbar.add(optionMenu);
        mbar.add(startItem);
        mbar.add(stepItem);
        mbar.add(userItem);
        mbar.add(colorItem);
        mbar.add(clearItem);
        mbar.add(sizeMenu);
        mbar.add(speedMenu);
    }

    // build menu of grid size options
    private JMenu buildSizeMenu() {
        JMenu sizeMenu = new JMenu("Size");
        sizeMenu.add(new SizeMenuItem(10, 10));
        sizeMenu.add(new SizeMenuItem(20, 20));
        sizeMenu.add(new SizeMenuItem(30, 30));
        sizeMenu.add(new SizeMenuItem(40, 40));
        sizeMenu.add(new SizeMenuItem(50, 50));
        sizeMenu.add(new SizeMenuItem(75, 75));
        sizeMenu.add(new SizeMenuItem(100, 100));

        // allows user to input custom width and height
        JMenuItem customSize = new JMenuItem("Custom...");
        customSize.addActionListener(e -> {
            try {
                int w = Integer.parseInt(JOptionPane.showInputDialog(this,
                        "Enter the width:"));
                int h = Integer.parseInt(JOptionPane.showInputDialog(this,
                        "Enter the height:"));
                sizeMenu.remove(customSize);
                // adds custom size to menu for selection
                sizeMenu.add(new SizeMenuItem(h, w));
                sizeMenu.add(customSize);
            } catch (java.lang.NumberFormatException n) {
                JOptionPane.showMessageDialog(this, "Invalid Number");
            }
        });
        sizeMenu.add(customSize);
        return sizeMenu;
    }

    // builds menu of speed options
    private JMenu buildSpeedMenu() {
        JMenu speedMenu = new JMenu("Speed");
        speedMenu.add(new SpeedMenuItem(10));
        speedMenu.add(new SpeedMenuItem(25));
        speedMenu.add(new SpeedMenuItem(50));
        speedMenu.add(new SpeedMenuItem(100));
        speedMenu.add(new SpeedMenuItem(200));
        speedMenu.add(new SpeedMenuItem(500));

        // allows user to input custom speed in milliseconds
        JMenuItem customSpeed = new JMenuItem("Custom...");
        customSpeed.addActionListener(e -> {
            try {
                int h = Integer.parseInt(JOptionPane.showInputDialog(this,
                        "Enter the speed:"));
                speedMenu.remove(customSpeed);
                // adds custom speed to menu for selection
                speedMenu.add(new SpeedMenuItem(h));
                speedMenu.add(customSpeed);
            } catch (java.lang.NumberFormatException n) {
                JOptionPane.showMessageDialog(this, "Invalid Number");
            }
        });
        speedMenu.add(customSpeed);
        return speedMenu;
    }

    // builds menu of option options
    private JMenu buildOptionMenu() {
        // initializes items
        JMenu optionMenu = new JMenu("Options");
        JMenuItem contiguousItem = new JMenuItem("Contiguous: "
                + (gameState.isContiguous() ? "On" : "Off"));
        JMenuItem randomItem = new JMenuItem("Random Start...");
        JMenuItem secondItem = new JMenuItem("Change Second Strain Rules...");
        // sets keyboard shortcuts for option options
        contiguousItem.setAccelerator(KeyStroke.getKeyStroke("control C"));
        randomItem.setAccelerator(KeyStroke.getKeyStroke("control shift N"));
        secondItem.setAccelerator(KeyStroke.getKeyStroke("control shift R"));
        // sets actions for item contiguous
        contiguousItem.addActionListener(e -> {
            gameState.setContiguous(!gameState.isContiguous());
            contiguousItem.setText("Contiguous: "
                    + (gameState.isContiguous() ? "On" : "Off"));
        });
        // Sets a random configuration on the board.
        randomItem.addActionListener(e -> {
            try {
                // Prompt the user for the desired population density
                String message = "Enter population density (<1)";
                JCheckBox checkbox = new JCheckBox("Two strain?");
                String density;
                if (gameState.isSecond())
                    density = JOptionPane.showInputDialog(this, checkbox,
                            message, JOptionPane.PLAIN_MESSAGE);
                else
                    density = JOptionPane.showInputDialog(this, message,
                            JOptionPane.PLAIN_MESSAGE);
                boolean twoStrain = checkbox.isSelected();
                double r = 1;
                if (density != null)
                    r = Double.parseDouble(density);
                // checks if the number is valid
                if (r < 1) {
                    // clear the current grid
                    int[][] grid = new int[gameState.getySize()][gameState
                            .getxSize()];
                    gameState.setGrid(grid);
                    // initialize new Random object to generate random numbers
                    Random rand = new Random();
                    // initialize lists of X and Y coordinates
                    ArrayList<Integer> newXs = new ArrayList<>();
                    ArrayList<Integer> newYs = new ArrayList<>();
                    for (int i = 0; i < (int) (gameState.getxSize()
                            * gameState.getySize() * r); i++) {
                        // generates new random x and y and checks if it is used
                        // before
                        int tempX = rand.nextInt(gameState.getxSize());
                        int tempY = rand.nextInt(gameState.getySize());
                        boolean newP = !newXs.contains(tempX);
                        if (!newP) {
                            newP = !(newYs.get(newXs.indexOf(tempX)) == tempY);
                        }
                        if (newP) {
                            // if the point has not been used, then it is added
                            // to the list of random points
                            newXs.add(tempX);
                        }
                        newYs.add(tempY);
                        // random point is added to the grid, and moves on to
                        // the next point
                        // if the user wants a two strain random grid, then it
                        // chooses a 1 or 2 randomly
                        gameState.getGrid()[tempY][tempX] = (twoStrain) ? rand
                                .nextInt(2) + 1 : 1;
                    }
                    gridPanel.repaint();
                }
                // if the number is not above 1 or if the answer is not a number
                // then it prompts the user with an error message
                else if (density != null)
                    JOptionPane.showMessageDialog(this, "Invalid Density");
            } catch (java.lang.NumberFormatException n) {
                JOptionPane.showMessageDialog(this, "Invalid Density");
            }
        });
        // Allow users to toggle the different second strain rules
        secondItem.addActionListener(e -> {
            JCheckBox secondReproduce = new JCheckBox("Reproduction Rule");
            JCheckBox secondDie = new JCheckBox("Kill Rule");

            // get the current rule preferences
                secondReproduce.setSelected(gameState.getSecondReproduce());
                secondDie.setSelected(gameState.getSecondDie());

                Object[] params = { secondReproduce, secondDie };

                int n = JOptionPane.showConfirmDialog(this, params,
                        "Second Strain Rules:", JOptionPane.OK_CANCEL_OPTION);
                if (n == JOptionPane.OK_OPTION) {
                    gameState.setSecondReproduce(secondReproduce.isSelected());
                    gameState.setSecondDie(secondDie.isSelected());
                    // If all second strain rules are disabled, turn off the
                    // second user
                if (!gameState.isSecond()) {
                    // switch back to user 1 if it isn't
                    if (gameState.getCurrentUser() != 1)
                        gameState.switchCurrentUser();
                    // reassigns all user 2 cells to user 1
                    int[][] tempGrid = new int[gameState.getySize()][gameState
                            .getxSize()];
                    for (int y = 0; y < gameState.getySize(); y++)
                        for (int x = 0; x < gameState.getxSize(); x++)
                            tempGrid[y][x] = gameState.getGrid()[y][x] == 2 ? 1
                                    : gameState.getGrid()[y][x];
                    gameState.setGrid(tempGrid);
                }
                gridPanel.repaint();
            }
        });
        // adds the items and returns menu
        optionMenu.add(contiguousItem);
        optionMenu.add(randomItem);
        optionMenu.add(secondItem);
        return optionMenu;
    }

    // builds menu of file options
    private JMenu buildFileMenu() {
        // initializes items
        JMenu fileMenu = new JMenu("File");
        JMenuItem openItem = new JMenuItem("Open");
        JMenuItem saveItem = new JMenuItem("Save");
        JMenuItem aboutItem = new JMenuItem("About");
        JMenuItem quitItem = new JMenuItem("Quit");
        // sets keyboard shortcuts for file options
        openItem.setAccelerator(KeyStroke.getKeyStroke("control O"));
        saveItem.setAccelerator(KeyStroke.getKeyStroke("control S"));
        quitItem.setAccelerator(KeyStroke.getKeyStroke("control shift Q"));
        // sets actions for items open, save, and quit
        openItem.addActionListener(e -> fileWriter.openFile());
        saveItem.addActionListener(e -> cropGrid());
        aboutItem.addActionListener(e -> JOptionPane.showMessageDialog(this,
                "Multiplayer Game of Life\n\n"
                        + "Version: 1.12 - 10 February 2015\n"
                        + "Creators: Micah Halter & Cody Biedermann\n ",
                "About", JOptionPane.INFORMATION_MESSAGE));
        quitItem.addActionListener(e -> System.exit(0));
        // adds the items and returns menu
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(aboutItem);
        fileMenu.add(quitItem);
        return fileMenu;
    }

    // method to resize the current grid
    private int[][] resizeGrid(int newYSize, int newXSize) {
        // initializes temporary grid of new size
        int[][] newGrid = new int[newYSize][newXSize];
        // translates the old grid to the new
        for (int y = 0; y < gameState.getySize() && y < newYSize - 1; y++)
            for (int x = 0; x < gameState.getxSize() && x < newXSize - 1; x++)
                newGrid[y][x] = gameState.getGrid()[y][x];
        // resets the x and y size variables and returns the new grid
        gameState.setySize(newYSize);
        gameState.setxSize(newXSize);
        return newGrid;
    }

    // crops grid to boundaries of current live cells and saves to file
    private void cropGrid() {
        // initializes variables used
        int firstRow = -1;
        int lastRow = -1;
        int firstCol = -1;
        int lastCol = -1;

        // loops through and marks the x and y boundaries of the live cells
        for (int y = 0; y < gameState.getySize(); y++)
            for (int x = 0; x < gameState.getxSize(); x++)
                if (gameState.getGrid()[y][x] != 0) {
                    firstRow = ((firstRow == -1) || (y < firstRow)) ? y
                            : firstRow;
                    firstCol = ((firstCol == -1) || (x < firstCol)) ? x
                            : firstCol;
                    lastRow = (y > lastRow) ? y : lastRow;
                    lastCol = (x > lastCol) ? x : lastCol;
                }

        // checks to make sure the grid wasn't blank
        if (firstRow != -1) {
            // creates temporary grid of the specified boundary and fills with
            // the
            // cells
            int[][] cGrid = new int[lastRow + 1][lastCol + 1];
            for (int y = firstRow; y <= lastRow; y++)
                cGrid[y] = gameState.getGrid()[y].clone();
            // sends the file off for saving
            fileWriter.writeToFile(cGrid, firstCol, firstRow, lastCol, lastRow);
        }
    }

    class SizeMenuItem extends JMenuItem {

        private static final long serialVersionUID = 5500403689725866488L;
        private final int x, y;

        public SizeMenuItem(int _y, int _x) {
            // initializes name and variables
            super(_x + "x" + _y);
            y = _y;
            x = _x;
            // adds action listener to resize grid to the correct dimensions and
            // repaint
            addActionListener(e -> {
                gameState.setGrid(resizeGrid(y, x));
                gridPanel.repaint();
            });
        }
    }

    // speed menu item class for easy addition of new speed menu items
    class SpeedMenuItem extends JMenuItem {

        private static final long serialVersionUID = -2630048822872351199L;

        public SpeedMenuItem(int _speed) {
            // initializes name
            super(_speed + "ms");
            // adds action listener to change the timer speed to speed up
            // animation
            addActionListener(e -> gameState.setGameTimer(new Timer(_speed,
                    (f) -> gameOfLife.nextStep())));
        }
    }
}